from django.urls import path

from .views import HomeView, CarCreateView

app_name = 'common_app'
urlpatterns = [
    path('', HomeView.as_view(), name='home'),
    path('car_create/', CarCreateView.as_view(), name='car_create')
]
