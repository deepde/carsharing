from django.utils import translation


def set_language_from_request(request):
    user = request.user
    if hasattr(user, 'language'):
        translation.activate(user.language)
