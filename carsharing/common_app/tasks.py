from celery import task
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from cars_app.models import UserCar


@task(task_time_limit=60)
def user_car_notification(obj_id):
    obj = UserCar.objects.get(id=obj_id)
    context = {
        'user': obj.user,
        'car': obj.car,
    }
    subject, from_email, to = 'Car booking', 'robot@carsharing.com', obj.user.email
    html_content = render_to_string('common_app/emails/new_booking.html', context)
    msg = EmailMessage(subject, html_content, from_email, [to])
    msg.content_subtype = "html"
    msg.send()
