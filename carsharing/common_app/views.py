from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, FormView

from cars_app.models import Car, UserCar, CarNameTranslation
from cars_app.forms import UserCarForm, CarCreateForm
from .tasks import user_car_notification

User = get_user_model()


class HomeView(LoginRequiredMixin, FormView, ListView):
    model = UserCar
    template_name = "common_app/home.html"
    form_class = UserCarForm

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.user = self.request.user
        obj.save()
        # User notification.
        user_car_notification(obj.id)

        return HttpResponseRedirect(reverse('common_app:home'))

    def get_queryset(self):
        queryset = super(HomeView, self).get_queryset()
        queryset = queryset.filter(user=self.request.user)
        return queryset


class CarCreateView(LoginRequiredMixin, FormView, ListView):
    model = Car
    template_name = "common_app/car_create.html"
    form_class = CarCreateForm

    def form_valid(self, form):
        obj = form.save()
        obj.save()

        return HttpResponseRedirect(reverse('common_app:car_create'))
