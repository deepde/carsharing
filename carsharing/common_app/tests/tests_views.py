from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

from cars_app.models import Car

User = get_user_model()


class TestView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(email='user@test.com', password='secret', first_name='user', language='ru')
        self.car = Car.objects.create(manufacture_year='2000')


class TestHomeView(TestView):
    url = reverse('common_app:home')

    def test_list_view(self):
        # test req method GET, user is not login
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

        # test req method GET, user is login
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'common_app/home.html')

    def test_form_view(self):
        req_data = {
            'car': self.car.pk,
        }
        self.client.force_login(self.user)
        response = self.client.post(self.url, req_data)
        self.assertRedirects(response, self.url, status_code=302, target_status_code=200, msg_prefix='',
                             fetch_redirect_response=True)


class TestCarCreateView(TestView):
    url = reverse('common_app:car_create')

    def test_list_view(self):
        # test req method GET, user is not login
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)

        # test req method GET, user is login
        self.client.force_login(self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'common_app/car_create.html')

    def test_form_view(self):
        req_data = {
            'manufacture_year': '2000',
            'car_name_ru': 'ru name',
            'car_name_en': 'en name',
        }
        self.client.force_login(self.user)
        response = self.client.post(self.url, req_data)
        self.assertRedirects(response, self.url, status_code=302, target_status_code=200, msg_prefix='',
                             fetch_redirect_response=True)
