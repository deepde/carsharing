import datetime

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import get_language

User = get_user_model()

MIN_CAR_YEAR = 1990


def year_choices():
    return [(r, r) for r in range(MIN_CAR_YEAR, datetime.date.today().year)]


class Car(models.Model):
    manufacture_year = models.IntegerField(_('year of car manufacture'), choices=year_choices())
    date_added = models.DateTimeField(_('date added'), default=timezone.now)

    def __str__(self):
        return self.name

    @property
    def name(self):
        language = get_language()
        try:
            trans = self.name_trans.get(language=language)
        except CarNameTranslation.DoesNotExist:
            return ""
        return trans.name


class CarNameTranslation(models.Model):
    language = models.CharField(_('language'), max_length=2, choices=settings.LANGUAGES)
    car = models.ForeignKey(Car, related_name='name_trans', on_delete=models.CASCADE)
    name = models.CharField(_('Name'), max_length=50)



class UserCar(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_cars')
    car = models.ForeignKey(Car, on_delete=models.CASCADE, related_name='user_cars')

    class Meta:
        unique_together = ("user", "car")

    def __str__(self):
        return self.car.name

    @property
    def user_name(self):
        return self.user.first_name

    @property
    def user_language(self):
        return self.user.language
