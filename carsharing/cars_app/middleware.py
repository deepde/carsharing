from django.utils.deprecation import MiddlewareMixin
from common_app.utils import set_language_from_request


class SetUserLanguage(MiddlewareMixin):
    def process_request(self, request):
        set_language_from_request(request)
