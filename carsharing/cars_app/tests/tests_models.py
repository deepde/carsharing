from django.contrib.auth import get_user_model
from django.test import TestCase

from cars_app.models import Car, CarNameTranslation, UserCar
from django.utils.translation import activate, get_language

User = get_user_model()


class TestModel(TestCase):
    def setUp(self):
        self.user = User.objects.create(email='user@test.com', password='secret', first_name='user', language='ru')
        self.car = Car.objects.create(manufacture_year='2000')
        self.user_car = UserCar.objects.create(user=self.user, car=self.car)


class TestCarModel(TestModel):
    def test_get_car(self):
        self.assertEqual(Car.objects.get(), self.car)


class TestCarNameTranslationModel(TestModel):
    def test_name_translation(self):
        original_lang = get_language()

        car = Car.objects.create(manufacture_year='2000')
        self.assertEqual(car.name, '')
        self.assertEqual(str(car), '')

        car.name_trans.create(language='en', name='en name')
        try:
            activate('en')
            self.assertEqual(car.name, 'en name')
            self.assertEqual(str(car), 'en name')
            activate('ru')
            self.assertEqual(car.name, '')
            self.assertEqual(str(car), '')
        finally:
            activate(original_lang)

        car.name_trans.create(language='ru', name='ru name')
        try:
            activate('ru')
            self.assertEqual(car.name, 'ru name')
            self.assertEqual(str(car), 'ru name')
        finally:
            activate(original_lang)


class TestUserCarModel(TestModel):
    def test_get_car(self):
        self.assertEqual(UserCar.objects.get(), self.user_car)
