from django.test import TestCase

from cars_app.forms import CarCreateForm, UserCarForm
from cars_app.models import Car


class TestCarCreateForm(TestCase):
    def test_user_creation_form(self):
        # test invalid data
        invalid_data = {
            'manufacture_year': '2900',
        }
        form = CarCreateForm(data=invalid_data)
        form.is_valid()
        self.assertTrue(form.errors)

        # test valid data
        valid_data = {
            'manufacture_year': '2000',
            'car_name_ru': 'тестовая машина',
            'car_name_en': 'test car',
        }
        form = CarCreateForm(data=valid_data)
        form.is_valid()
        self.assertFalse(form.errors)


class TestUserCarForm(TestCase):
    def setUp(self):
        self.car = Car.objects.create(manufacture_year='2000')

    def test_user_creation_form(self):
        # test invalid data
        invalid_data = {'car': ''}
        form = UserCarForm(data=invalid_data)
        form.is_valid()
        self.assertTrue(form.errors)

        # test valid data
        valid_data = {'car': self.car.pk}
        form = UserCarForm(data=valid_data)
        form.is_valid()
        self.assertFalse(form.errors)
