from django import forms
from django.conf import settings

from .models import Car, UserCar


class UserCarForm(forms.ModelForm):
    class Meta:
        model = UserCar
        fields = ("car",)
        widgets = {
            'car': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(UserCarForm, self).__init__(*args, **kwargs)
        # Prefetch translations to call db once.
        self.fields['car'].queryset = self.fields['car'].queryset.prefetch_related('name_trans')


class CarCreateForm(forms.ModelForm):
    car_name_field_pattern = 'car_name_{}'

    class Meta:
        model = Car
        fields = ("manufacture_year",)
        widgets = {
            'manufacture_year': forms.Select(attrs={'class': 'form-control'}),
        }

    def __init__(self, *args, **kwargs):
        super(CarCreateForm, self).__init__(*args, **kwargs)

        for lang in settings.LANGUAGES:
            lang_code = lang[0]
            lang_name = lang[1]
            field_name = self.car_name_field_pattern.format(lang_code)
            field_label = 'Car Name ({})'.format(lang_name)
            self.fields[field_name] = forms.CharField(label=field_label,
                                                      widget=forms.TextInput(attrs={'class': 'form-control'}),
                                                      required=True)

    def save(self, commit=True):
        car = super(CarCreateForm, self).save(commit=commit)
        if commit:
            # Always create name translations on form submission.
            for lang in settings.LANGUAGES:
                lang_code = lang[0]
                field_name = self.car_name_field_pattern.format(lang_code)
                car_name = self.cleaned_data.get(field_name)
                if car_name:
                    car.name_trans.create(language=lang_code, name=car_name)
        return car
