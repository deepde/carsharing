from django.contrib import admin

from .models import Car, UserCar, CarNameTranslation


class CarAdmin(admin.ModelAdmin):
    list_display = ['name', 'manufacture_year', 'date_added']


class CarNameTranslationAdmin(admin.ModelAdmin):
    list_display = ['language', 'car', 'name']


class UserCarAdmin(admin.ModelAdmin):
    list_display = ['user', 'car']


admin.site.register(Car, CarAdmin)
admin.site.register(CarNameTranslation, CarNameTranslationAdmin)
admin.site.register(UserCar, UserCarAdmin)
