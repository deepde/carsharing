from django.contrib.auth import get_user_model
from rest_framework import status
from rest_framework.test import APITestCase, APIClient

from cars_app.models import Car, CarNameTranslation, UserCar
from rest_framework.utils import json

User = get_user_model()


class UserTests(APITestCase):
    def setUp(self):
        self.user = User.objects.create(email='user@test.com', password='secret', first_name='user', language='ru')
        self.car = Car.objects.create(manufacture_year='2000')
        self.car.name_trans.create(language='ru', name='ru name')
        self.car.name_trans.create(language='en', name='en name')
        self.user_car = UserCar.objects.create(user=self.user, car=self.car)

    def test_create_user(self):
        url = '/api/v1/signup/'
        data = {'email': 'user2@test.com',
                'first_name': 'user2',
                'language': 'ru',
                'password': 'secret'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.count(), 2)
        self.assertEqual(User.objects.get(pk=2).first_name, 'user2')

    def test_users_list(self):
        url = '/api/v1/users/'
        user = User.objects.get(email='user@test.com')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content),
                         [{'id': 5, 'email': 'user@test.com', 'first_name': 'user', 'language': 'ru'}])

    def test_user_detail(self):
        url = '/api/v1/users/4/'
        user = User.objects.get(email='user@test.com')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.get(url, data={})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content),
                         {'id': 4, 'email': 'user@test.com', 'first_name': 'user', 'language': 'ru'})

    def test_user_cars(self):
        url = '/api/v1/users/3/cars/'
        user = User.objects.get(email='user@test.com')
        client = APIClient()
        client.force_authenticate(user=user)
        response = client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(json.loads(response.content), [{'name': 'ru name', 'manufacture_year': 2000}])
