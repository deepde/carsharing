from django.conf.urls import url
from .views import SignupView, ChangePasswordView, UsersView, UserView, UserCarsView
from rest_framework.authtoken import views

app_name = 'api_app'
urlpatterns = [
    url(r'^login/$', views.obtain_auth_token),
    url(r'^signup/$', SignupView.as_view()),
    url(r'^change_password/$', ChangePasswordView.as_view()),
    url(r'^users/(?P<pk>\d+)/cars/$', UserCarsView.as_view()),
    url(r'^users/(?P<pk>\d+)/$', UserView.as_view()),
    url(r'^users/$', UsersView.as_view()),
]
