from django.contrib.auth.hashers import check_password

from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from cars_app.models import Car

User = get_user_model()


class SignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'password', 'language')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        user = User.objects.create_user(email=validated_data['email'],
                                        first_name=validated_data['first_name'],
                                        language=validated_data['language'],
                                        password=validated_data['password'])
        # Generate token for user
        Token.objects.create(user=user)
        return user


class ChangePasswordSerializer(serializers.Serializer):
    old_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def create(self, validated_data):
        request = self.context.get('request')
        user = request.user

        if not check_password(validated_data['old_password'], user.password):
            raise serializers.ValidationError({'old_password': ['Old password does not match']})

        user.set_password(validated_data['new_password'])
        return user


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('id', 'email', 'first_name', 'language',)
        read_only_fields = ('id',)
        model = User


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        fields = ('name', 'manufacture_year')
        model = Car
