from django.contrib.auth import get_user_model
from django.http import Http404
from rest_framework.views import APIView
from rest_framework import generics, permissions, authentication

from cars_app.models import UserCar, Car
from common_app.utils import set_language_from_request
from .serializers import SignupSerializer, ChangePasswordSerializer, UserSerializer, CarSerializer


User = get_user_model()


class BaseAPIView(APIView):
    def initial(self, request, *args, **kwargs):
        super(BaseAPIView, self).initial(request, *args, **kwargs)
        set_language_from_request(request)


# Get and update User Detail
class UserView(BaseAPIView, generics.RetrieveAPIView, generics.UpdateAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


# Get all users
class UsersView(BaseAPIView, generics.ListAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = User.objects.all()
    serializer_class = UserSerializer


# Get user cars
class UserCarsView(BaseAPIView, generics.ListAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Car.objects.all()
    serializer_class = CarSerializer

    def filter_queryset(self, queryset):
        try:
            user = User.objects.get(pk=self.kwargs['pk'])
        except User.DoesNotExist:
            raise Http404()
        return queryset.filter(user_cars__user=user)


# Signup
class SignupView(BaseAPIView, generics.CreateAPIView):
    authentication_classes = ()
    permission_classes = (permissions.AllowAny,)
    serializer_class = SignupSerializer


# Change password
class ChangePasswordView(BaseAPIView, generics.CreateAPIView):
    authentication_classes = (authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ChangePasswordSerializer
