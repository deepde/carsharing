
// Move labels for not empty inputs
document.addEventListener("DOMContentLoaded", function(e) {
   var inputs = document.forms["form"].getElementsByTagName("input");
   for ( i = 0; i < inputs.length; i++) {
       if (inputs[i].value.length > 0) {
           inputs[i].parentNode.classList.add('filled');
       }
   }

});


// Move labels on inputs focus
function InputFocusIn(e) {
    if (isInput(e)) {
        e.srcElement.parentNode.classList.add('focused');
    }
}
function InputFocusOut(e) {
    if (isInput(e)) {
        var value = e.srcElement.value;
        if (value.length > 0) {
            e.srcElement.parentNode.classList.add('filled');
        } else {
            e.srcElement.parentNode.classList.remove('filled');
        }
        e.srcElement.parentNode.classList.remove('focused');
    }
}
function isInput(e) {
    var e = window.e || e;

    return e.target.tagName === 'INPUT';
}
document.addEventListener('focusin', InputFocusIn, false);
document.addEventListener('focusout', InputFocusOut, false);


// Show or hide password
function show() {
    var p = document.getElementById('id_password');
    p.setAttribute('type', 'text');
    var l = document.getElementById('eye');
    l.innerHTML = '<i class="far fa-eye-slash"></i>'
}
function hide() {
    var p = document.getElementById('id_password');
    p.setAttribute('type', 'password');
    var l = document.getElementById('eye');
    l.innerHTML = '<i class="far fa-eye"></i>';
}

var pwShown = 0;

document.getElementById("eye").addEventListener("click", function () {
    if (pwShown === 0) {
        pwShown = 1;
        show();
    } else {
        pwShown = 0;
        hide();
    }
}, false);