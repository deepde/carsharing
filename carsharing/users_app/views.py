from django.contrib.auth import login, logout, authenticate
from django.contrib.auth import views as auth_views
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy, reverse
from django.utils.translation import ugettext_lazy as _
from django.views import View
from django.views.generic import FormView, UpdateView, DetailView

from .forms import UserCreationForm
from .models import User


class SignUpFormView(FormView):
    form_class = UserCreationForm
    success_url = reverse_lazy('common_app:home')
    template_name = "users_app/signup.html"

    def form_valid(self, form):
        form.save()
        email = form.cleaned_data['email']
        password = form.cleaned_data['password']
        user = authenticate(email=email, password=password)
        login(self.request, user)
        return super(SignUpFormView, self).form_valid(form)


class SignInFormView(FormView):
    form_class = AuthenticationForm
    template_name = "users_app/login.html"
    success_url = reverse_lazy('common_app:home')

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(SignInFormView, self).form_valid(form)


class LogoutView(View):
    def get(self, request):
        logout(request)
        return HttpResponseRedirect(reverse('common_app:home'))


class PasswordChangeView(auth_views.PasswordChangeView):
    template_name = 'users_app/password_change.html'
    form_valid_message = _("Your password was changed!")
    success_url = reverse_lazy('users_app:user_detail')


class PasswordResetView(auth_views.PasswordResetView):
    template_name = 'users_app/password_reset.html'
    success_url = reverse_lazy('users_app:password_reset_done')
    email_template_name = 'users_app/emails/password_reset.html'


class PasswordResetDoneView(auth_views.PasswordResetDoneView):
    template_name = 'users_app/password_reset_done.html'


class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    template_name = 'users_app/password_reset_confirm.html'
    success_url = reverse_lazy('common_app:home')
    form_valid_message = _("Your password was changed!")

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User

    def get_object(self, *args, **kwargs):
        return self.request.user


class UserUpdateView(LoginRequiredMixin, UpdateView):
    model = User
    fields = ['email', 'first_name', 'language']
    template_name = 'users_app/user_update.html'
    success_url = reverse_lazy('users_app:user_detail')

    def get_object(self, *args, **kwargs):
        return self.request.user
