from django import forms
from django.contrib.auth import get_user_model
from django.forms import TextInput, EmailInput, Select
from django.utils.translation import ugettext_lazy as _

from .tasks import user_welcome

User = get_user_model()


class UserCreationForm(forms.ModelForm):
    password = forms.CharField(label=_(u"Пароль"),
                               widget=forms.PasswordInput(attrs={'class': 'form-control'}), required=True)

    class Meta:
        model = User
        fields = ("email", "first_name", "language")
        widgets = {
            'first_name': TextInput(attrs={'class': 'form-control'}),
            'language': Select(attrs={'class': 'form-control'}),
            'email': EmailInput(attrs={'class': 'form-control'}),
            'password': TextInput(attrs={'class': 'form-control'}),
        }

    def clean_password(self):
        password = self.cleaned_data.get('password')
        if len(password) < 6:
            raise forms.ValidationError(_('Password too short'))
        return password

    def save(self, request=None, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])

        if commit:
            user.save()
            # User notification
            user_welcome(user.id)

        return user
