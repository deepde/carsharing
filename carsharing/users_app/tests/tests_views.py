from django.contrib.auth import get_user_model
from django.test import TestCase, Client
from django.urls import reverse

User = get_user_model()


class TestSignupFormView(TestCase):
    def setUp(self):
        self.client = Client()

    def test_signup(self):
        url = reverse('users_app:signup')

        # test req method GET
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users_app/signup.html')

        # test req method POST with empty data
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-danger')

        # test req method POST with invalid data
        req_data = {
            'email': 'user@test.com',
            'password': '',
            'first_name': 'user',
            'language': 'ru',
        }
        response = self.client.post(url, req_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-danger')

        # test req method POST with valid data
        req_data = {
            'email': 'user@test.com',
            'password': 'secret',
            'first_name': 'user',
            'language': 'ru',
        }
        response = self.client.post(url, req_data)
        self.assertRedirects(response, reverse('common_app:home'),
                             status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)


class TestSignInFormView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(email='user@test.com', password='secret', first_name='user', language='ru')

    def test_sign_in(self):
        url = reverse('users_app:login')

        # test req method GET
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users_app/login.html')

        # test req method POST with empty data
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-danger')

        # test req method POST with invalid data
        req_data = {
            'username': 'user@test.com',
            'password': 'secret1',
        }
        response = self.client.post(url, req_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-danger')

        # test req method POST with valid data
        req_data = {
            'username': 'user@test.com',
            'password': 'secret',
        }
        response = self.client.post(url, req_data)
        self.assertEqual(response.status_code, 200)


class TestUserUpdateView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(email='user@test.com', password='secret', first_name='user', language='ru')

    def test_update(self):
        url = reverse('users_app:update_user')

        # test req method GET, user is not login
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        # test req method GET, user is login
        # self.client.login(username='user@test.com', password='secret') # ???
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users_app/user_update.html')

        # test req method POST with empty data
        response = self.client.post(url, {})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-danger')

        # test req method POST with invalid data
        req_data = {
            'email': 'usertest',
            'language': '',
            'first_name': 'new_name',
        }
        response = self.client.post(url, req_data)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'alert-danger')

        # test req method POST with valid data
        req_data = {
            'email': 'user@test.com',
            'language': 'en',
            'first_name': 'new_name',
        }
        response = self.client.post(url, req_data)
        self.assertRedirects(response, reverse('users_app:user_detail'),
                             status_code=302, target_status_code=200, msg_prefix='', fetch_redirect_response=True)


class TestUserDetailView(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(email='user@test.com', password='secret', first_name='user', language='ru')

    def test_detail(self):
        url = reverse('users_app:user_detail')

        # test req method GET, user is not login
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

        # test req method GET, user is login
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'users_app/user_detail.html')
