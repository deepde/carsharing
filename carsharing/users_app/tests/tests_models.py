from django.contrib.auth import get_user_model
from django.test import TestCase

User = get_user_model()


class TestUserModel(TestCase):
    def setUp(self):
        self.user = User.objects.create(email='user@test.com', password='secret', first_name='user', language='ru')

    def test_get_user(self):
        self.assertEqual(User.objects.get(), self.user)
