from django.test import TestCase

from users_app.forms import UserCreationForm


class TestUserCreationForm(TestCase):
    def test_user_creation_form(self):
        # test invalid data
        invalid_data = {
            'first_name': '',
            'email': '',
            'language': '',
            'password': ''
        }
        form = UserCreationForm(data=invalid_data)
        self.assertFalse(form.is_valid())
        self.assertIn('first_name', form.errors)
        self.assertIn('email', form.errors)
        self.assertIn('language', form.errors)
        self.assertIn('password', form.errors)

        # test valid data
        valid_data = {
            'first_name': 'user',
            'email': 'user@test.com',
            'language': 'ru',
            'password': 'secret'
        }
        form = UserCreationForm(data=valid_data)
        self.assertTrue(form.is_valid())
