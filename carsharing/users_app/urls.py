from django.contrib.auth.views import PasswordResetCompleteView
from django.urls import path

from . import views

app_name = 'users_app'
urlpatterns = [

    path('signup/', views.SignUpFormView.as_view(), name='signup'),
    path('login/', views.SignInFormView.as_view(), name='login'),
    path('logout/', views.LogoutView.as_view(), name='logout'),
    path('update/', views.UserUpdateView.as_view(), name='update_user'),
    path('password_change/', views.PasswordChangeView.as_view(), name='password_change'),
    path('password_reset/', views.PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('', views.UserDetailView.as_view(), name='user_detail'),

]
