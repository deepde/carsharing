from celery import task
from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from .models import User


@task(task_time_limit=60)
def user_welcome(user_id):
    user = User.objects.get(id=user_id)
    context = {
        'user': user,
    }
    subject, from_email, to = 'Welcome', 'robot@carsharing.com', user.email
    html_content = render_to_string('users_app/emails/welcome.html', context)
    msg = EmailMessage(subject, html_content, from_email, [to])
    msg.content_subtype = "html"
    msg.send()